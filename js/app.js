//set up menu items
(function(){
	var selectTab = function(page){
		//clear the currently elected tab
		$('.selected').removeClass('selected');

		//select the new tab so long as it isn't "home"
		if(page !=='#home'){
			$('header').find('a').each(function(){
				if($(this).attr('href') == page){
					$(this).addClass('selected');
				}
			});
		}
	}

	var hashUpdate = function(curr_hash) {
	    var page = curr_hash.substr(1);
	    loadPage(page);
	    selectTab(curr_hash);
	};

	function loadPage(page){
		$.ajax({
			url: 'body/' + page + '.html',
			success:function(data){
				$('#container').html(data);
			}
		});
	}

	$(window).bind('hashchange', function(e) {
	    hashUpdate(e.target.location.hash);
	});

	$('header').children('a').on('click', function(){
		selectTab($(this).parent());
	});

	$('#menu').on('click', function(){
		$('header').children('a').toggle();
		$(this).show();
		$('a').children('h1').parent('a').show();
	});

	//check for preexisting hash value
	if(!window.location.hash){
		//pre select home
		hashUpdate('#home');
	}
	// onload, if there was a fragment in the URL
	window.location.hash && hashUpdate(window.location.hash);
})();
